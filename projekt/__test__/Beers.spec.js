import React from 'react'
import { shallow, mount } from 'enzyme';
import renderer from 'react-test-renderer'
import CounterHome,{ Counter } from '../src/containers/Beers'
import configureStore from 'redux-mock-store'
import {Provider} from 'react-redux'
import {createStore, applyMiddleware} from 'redux'
import ReduxThunk from 'redux-thunk';

import {loadBeer, loadBeerDetails, removeBeerDetails} from '../src/actions/BeerActions'
import reducer from '../src/reducers'

const beerList = require('./mock/data.json');

// Snapshot for Home React Component
describe('BEERS --- Snapshot',() =>{
    it('+++capturing Snapshot of Home', () => {
        const renderedValue =  shallow(<Counter removeBeerDetails={removeBeerDetails} loadBeer={loadBeer(1)} loading={true} beers={{beerList}}/>)
        expect(renderedValue).toMatchSnapshot();
    });
});

//*******************************************************************************************************
describe('BEERS - REACT-REDUX (shallow)',()=>{
    const initialState = {
      beers: {
        beerDetail: {},
        beerList,
        loading: true,
        error: ''
      }
    }

    const mockStore = configureStore()
    let store,wrapper

    beforeEach(()=>{
        store = mockStore(initialState)
        wrapper = shallow(<CounterHome store={store} /> )
    })

    it('render the connected component', () => {
      expect(wrapper.length).toEqual(1)
    });

    it('check Prop matches with initialState', () => {
       expect(wrapper.prop('beerList')).toEqual(initialState.beerList)
    });

});

describe('BEERS - REACT-REDUX (Mount)',()=>{
    const initialState = {
      beers: {
        beerDetail: {},
        beerList,
        loading: true,
        error: ''
      }
    }

    let wrapper
    beforeEach(()=>{
        const store = createStore(reducer, initialState, applyMiddleware(ReduxThunk));
        wrapper = mount( <Provider store={store}><CounterHome /></Provider> )
    })

    it('render the connected component', () => {
       expect(wrapper.find(CounterHome).length).toEqual(1)
    });

    it('check Prop matches with initialState', () => {
       expect(wrapper.find(Counter).prop('beerList')).toEqual(initialState.beerList)
    });

});
