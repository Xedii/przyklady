import React from 'react'
import { shallow, mount } from 'enzyme';
import renderer from 'react-test-renderer'

import beerReducer from '../src/reducers/beerReducer'
const beerList = require('./mock/data.json')
const similarBeers = require('./mock/beerslist.json')

import {
    LOADING,
    REQUEST_LIST_SUCCESS,
    REQUEST_LIST_FAIL,
    REQUEST_DETAILS_SUCCESS,
    REQUEST_DETAILS_FAIL,
    REMOVE_DETAILS
} from '../src/actions/ActionType';

describe('REDUCER Test beerReducer',()=>{
    it('reducer for REQUEST_LIST_SUCCESS', () => {
      const beers = {
        beerDetail: {},
        beerList: '',
        loading: true,
        error: ''
      }

      const beersExepted = {
        beerDetail: {},
        beerList,
        loading: false,
        error: ''
      }
        const state = beerReducer(beers, {type:REQUEST_LIST_SUCCESS, payload:beerList})
        expect(state).toEqual(beersExepted)
    });

    it('reducer for REQUEST_DETAILS_SUCCESS', () => {
      const beers = {
        beerDetail: {},
        beerList: '',
        loading: false,
        error: ''
      }

      const beersExepted = {
        beerDetail: similarBeers,
        beerList:'',
        loading: false,
        error: ''
      }
        const state = beerReducer(beers, {type:REQUEST_DETAILS_SUCCESS, payload: similarBeers})
        expect(state).toEqual(beersExepted)
    });

    it('reducer for REMOVE_DETAILS', () => {

        const beers = {
          beerDetail: similarBeers,
          beerList:'',
          loading: false,
          error: ''
        }

        const beersExepted = {
          beerDetail: {},
          beerList: '',
          loading: false,
          error: ''
        }

        const state = beerReducer(beers, {type:REMOVE_DETAILS})
        expect(state).toEqual(beersExepted)
    });

});
