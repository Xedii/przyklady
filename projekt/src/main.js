import React from 'react';
import { render } from 'react-dom';
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import App from './components/App';
import reducer from './reducers';
import ReduxThunk from 'redux-thunk';

import 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.css';

const store = createStore(reducer, {}, applyMiddleware(ReduxThunk));

render(
    <Provider store={store}>
        <App />
    </Provider>,
    document.getElementById('root')
)
