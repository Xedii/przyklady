import React, { Component } from 'react';
import { connect } from 'react-redux';
import { loadBeer, loadBeerDetails, removeBeerDetails } from '../actions';
import InfiniteScroll from 'react-infinite-scroller';
import { Grid, Row, Col } from 'react-material-responsive-grid';
import SkyLight from 'react-skylight';
import Modal from '../components/commn/Modal';
import LazyLoad from 'react-lazyload';

export class Counter extends Component {
    constructor(props){
        super(props);

    }

    openDetailModal (beer) {
        this.props.loadBeerDetails(beer);
        this.showDialog.show()
    }

    renderList () {
      const { beerList } = this.props.beers;

      return beerList.map((beer, i) => {
        const {name, tagline, image_url } = beer;

          return (
              <Col xs4={4} lg={4} key={i}>
                <div onClick={this.openDetailModal.bind(this,beer)} className='card'>
                    <LazyLoad height={200} once >
                        <img className="imageCard" src={image_url} />
                    </LazyLoad>

                  <div className="container">
                    <p className="title">{name}</p>
                    <p className="title">{tagline}</p>
                  </div>
                </div>
              </Col>
          )
        });
    }

    render () {
        const loader = <div className="title">loading</div>;
        const { loading } = this.props.beers
        const { dialogStyle } = styles
        return (
          <InfiniteScroll
            loader={loader}
            pageStart={0}
            loadMore={this.props.loadBeer.bind(this)}
            hasMore={loading}>
            <div>
              <Grid>
                <Row>
                  { this.renderList() }
                </Row>
            </Grid>
          </div>
              <SkyLight
                  dialogStyles={dialogStyle}  hideOnOverlayClicked ref={ref => this.showDialog = ref}
                  afterClose={this.props.removeBeerDetails.bind(this)}>
                    <Modal/>
              </SkyLight>
          </InfiniteScroll>
        )
    }
}

const styles = {
    dialogStyle: {
        backgroundColor: '#ECEFF1',
        color: '#ffffff',
        width: '70%',
        height: '750px',
        marginTop: '-400px',
        marginLeft: '-35%',
        overflow: 'auto',
        position: 'fixed',
    }
};

const mapStateToProps = ({ beers }) => {
    return {
      beers
    };
};

export default connect(mapStateToProps,{loadBeer, loadBeerDetails, removeBeerDetails})(Counter);
