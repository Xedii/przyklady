import {
    LOADING,
    REQUEST_LIST_SUCCESS,
    REQUEST_LIST_FAIL,
    REQUEST_DETAILS_SUCCESS,
    REQUEST_DETAILS_FAIL,
    REMOVE_DETAILS
} from '../actions/ActionType';

const INITIAL_STATE = {
    beerDetail: {},
    beerList: [],
    loading: true,
    error: ''
};


export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case LOADING:
            return { ...state, loading: !state.loading};
        case REQUEST_DETAILS_SUCCESS:
            return { ...state, beerDetail: action.payload};
        case REMOVE_DETAILS:
            return { ...state, beerDetail: {}};
        case REQUEST_LIST_SUCCESS:
            return { ...state, beerList: [...state.beerList, ...action.payload], loading: false};
        case REQUEST_DETAILS_FAIL:
        case REQUEST_LIST_FAIL:
            return { ...state, error: 'sorry there is same error' };
        default:
            return state
    }
}
