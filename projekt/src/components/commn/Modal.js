import React, { Component } from 'react';
import { connect } from 'react-redux';
import LazyLoad from 'react-lazyload';
import { Grid, Row, Col } from 'react-material-responsive-grid';
import { Table } from 'react-bootstrap'

class Modal extends Component {

    renderNoSimilarBeers () {
      return (
          <Col xs4={4} lg={4} key={1}>
              <div >
                  <p className="title">There are not any similar beers</p>
              </div>
          </Col>
      )
    }

    renderSimilarBeers () {
        const { similarBeers } = this.props.beers.beerDetail;

        if(!Object.keys(similarBeers).length) return this.renderNoSimilarBeers()

        return similarBeers.map((beer, key) =>{
          const {name, tagline } = beer;
              return (
                  <Col xs4={4} lg={4} key={key}>
                      <div className='card'>
                          <LazyLoad height={200} once >
                              <img className="imageModal" src={beer.image_url} />
                          </LazyLoad>

                          <div className="container">
                              <p className="title">{ name }</p>
                              <p className="title">{ tagline }</p>
                          </div>
                      </div>
                  </Col>
              )
          })
    }

    render () {
        const { beerDetail } = this.props.beers;
        if(!Object.keys(beerDetail).length) return <div className="title"> Loading.... </div>

        const { image_url, name, tagline, description, brewers_tips, ibu, abv } = beerDetail.detail

        return (
            <div>
                <div className="text-center imgMargin">
                    <LazyLoad height={200} once >
                        <img className="imageCenterModal" src={image_url}/>
                    </LazyLoad>
                </div>

                <Table responsive>
                    <thead>
                    <tr className="table">
                        <th><p className="descriptionDetail">Name</p></th>
                        <th><p className="descriptionDetail">Tagline</p></th>
                        <th><p className="descriptionDetail">Ibu</p></th>
                        <th><p className="descriptionDetail">Abv</p></th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td><p className="Detail">{name}</p></td>
                        <td><p className="Detail">{tagline}</p></td>
                        <td><p className="Detail">{ibu}</p></td>
                        <td><p className="Detail">{abv}</p></td>
                    </tr>
                    </tbody>
                </Table>

                <div>
                    <Grid>
                        <Row>
                            <Col xs4={2} lg={2}>
                                <div className="container">
                                    <p className="descriptionDetail">Description: </p>
                                </div>
                            </Col>
                            <Col xs4={10} lg={10}>

                                <div className="container">
                                    <p className="Detail">{ description }</p>
                                </div>
                            </Col>
                            <Col xs4={2} lg={2}>
                                <div className="container">
                                    <p className="descriptionDetail">Brewers_tips: </p>
                                </div>
                            </Col>
                            <Col xs4={10} lg={10}>

                                <div className="container">
                                    <p className="Detail">{ brewers_tips }</p>
                                </div>
                            </Col>
                        </Row>
                    </Grid>
                </div>
                <div>
                    <Grid>
                        <Row>
                            { this.renderSimilarBeers() }
                        </Row>
                    </Grid>
                </div>
            </div>
        );
    }
}

const mapStateToProps = ({ beers }) => {
    return {
        beers
    };
};

export default connect(mapStateToProps)(Modal);
