import React from 'react';
import Beers from '../containers/Beers';

const App = () => {
    return (
        <div className="container">
            <Beers></Beers>
        </div>
    )
}
export default App;