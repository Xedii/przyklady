export const LOADING = 'loading';

export const REQUEST_LIST_SUCCESS = 'request_list_success';
export const REQUEST_LIST_FAIL = 'request_list_fail';

export const REQUEST_DETAILS_SUCCESS = 'request_details_success';
export const REQUEST_DETAILS_FAIL = 'request_details_fail';
export const REMOVE_DETAILS = 'remove_details';
