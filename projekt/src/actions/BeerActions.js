import axios from 'axios';
import {
    LOADING,
    REMOVE_DETAILS,
    REQUEST_LIST_SUCCESS,
    REQUEST_LIST_FAIL,
    REQUEST_DETAILS_SUCCESS,
    REQUEST_DETAILS_FAIL
} from './ActionType';

export const loadBeer = (page) => {
    return (dispatch) => {
        dispatch({ type: LOADING })
        axios.get(`https://api.punkapi.com/v2/beers?page=${page}&per_page=20`)
            .then(res => requestListSuccess(dispatch, res))
            .catch(err => requestListFail(dispatch))
    }
};

export const loadBeerDetails = (beer) => {
    return (dispatch) => {
        const ibuSimilar = 15;
        const ebcSimilar = 15;
        const abvSimilar = 1;
        const { ibu, ebc, abv } = beer;
        const params = {
            ibu_lt: isNotLessZero(ibu + ibuSimilar).toFixed(0),
            ibu_gt: isNotLessZero(ibu - ibuSimilar).toFixed(0),
            ebc_lt: isNotLessZero(ebc + ebcSimilar).toFixed(0),
            ebc_gt: isNotLessZero(ebc - ebcSimilar).toFixed(0),
            abv_lt: isNotLessZero(abv + abvSimilar).toFixed(0),
            abv_gt: isNotLessZero(abv - abvSimilar).toFixed(0)
        };
        const options = {
            method:'get',
            url: 'https://api.punkapi.com/v2/beers',
            params
        };

        axios(options)
            .then(res => requestDetailSuccess(dispatch, res, beer))
            .catch(err => requestDetailFail(dispatch))
    }
};

const isNotLessZero = obj => obj < 0 ? 0 : obj;

export const removeBeerDetails = () => {
    return {
        type: REMOVE_DETAILS
    }
};

const requestListSuccess = (dispatch, res) => {
    dispatch({
        type: REQUEST_LIST_SUCCESS,
        payload: res.data
    });
    if(res.data.length) dispatch({ type: LOADING });
};

const requestListFail = (dispatch) => {
    dispatch({
        type: REQUEST_LIST_FAIL
    });
};

const requestDetailSuccess = (dispatch, res, detail) => {
    const maxSimilarBeers = 3;
    const startSimilarBeer = 0;
    const isNotSame = beer => beer.name !== detail.name;

    const similarBeers = res.data.filter(isNotSame)
        .slice(startSimilarBeer, maxSimilarBeers);

    const beers = {
        detail,
        similarBeers
    };

    dispatch({
        type: REQUEST_DETAILS_SUCCESS,
        payload: beers
    });
};

const requestDetailFail = (dispatch) => {
    dispatch({
        type: REQUEST_DETAILS_FAIL
    });
};
