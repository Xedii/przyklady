const MongoClient = require('mongodb')

const getMongoURL = (options) => {
    const url = options.servers
        .reduce((prev, cur) => prev + `${cur}`, 'mongodb://')
        // .reduce((prev, cur) => prev + `${options.user}:${options.pass}@${cur}`, 'mongodb://')
    return `${url}/${options.db}`
}

const connect = (options, mediator) => {
    mediator.once('boot.ready', () => {
        MongoClient.connect(
            getMongoURL(options), {
                db: options.dbParameters(),
                server: options.serverParameters()
            }, (err, db) => {
                if (err) {
                    mediator.emit('db.error', err)
                }
                mediator.emit('db.ready',db)
            })
    })
}

module.exports = Object.assign({}, {connect})
