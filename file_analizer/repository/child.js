const csv = require('csv-streamify')
const fs = require('fs')
const parser = csv({ objectMode: true })
const bigXml = require('big-xml');

process.on('message', (msg,service) => {
  if(msg.msg === 'startAnalyze' && msg.type  === 'xml') startAnalyzeXml(msg)
  if(msg.msg === 'startAnalyze' && msg.type  === 'csv') startAnalyzeCsv(msg)
});

const startAnalyzeCsv = (data) => {
  parser.on('data', function (line) {
    process.send({product: line, status: 'progress'});
  })

  parser.on('end', function (line) {
    process.send({status: 'end'});
  })

  fs.createReadStream(`./${data.nameFile}`).pipe(parser)
}

const startAnalyzeXml = (data) => {
  const reader = bigXml.createReader(data.nameFile);

  reader.on('record', function(record) {
    console.log(record)
  });
}
