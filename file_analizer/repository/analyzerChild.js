const analyze = require('../analyzer')

process.on('message', (msg,service) => {
  if(msg.msg === 'analize') startAnalyzeCsv(msg)
});

const AnalyzeProduct = async (data) => {
    const analyzedProduct = await analyze(data)
    return process.send({product: analyzedProduct, status: 'done'});
}
