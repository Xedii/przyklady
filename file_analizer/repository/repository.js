'use strict'
const { fork } = require('child_process');

const repository = (container) => {
  const {database: db, ObjectID} = container.cradle
  const productCollection = db.collection('product')
  const analyzerForked = fork(`${__dirname}/analyzerChild.js`);

  analyzerForked.on('message', (msg) => {
    if(msg.status === 'done') saveProduct(msgs)
  });

  const analyze = (nameFile, type) => {
      const forked = fork(`${__dirname}/child.js`);
      forked.send({msg:'startAnalyze', type, nameFile, });

      forked.on('message', (msg) => {
        if(msg.status === 'end') console.log('end of validate data from child');
        if(msg.status === 'progress') getProductById(msg.product)
      });

      forked.on('close', function (code) {
         console.log('Child process is exiting with exit code: ', code);
      });
  }

  const updateStatus = (payload) => {
    return new Promise((resolve, reject) => {
      productCollection.updateOne({_id: new ObjectID(payload._id)},{ '$set':{status: payload.status} }, (err,status) => {
        if (err) reject(new Error('an error occured registring a save product, err:', err))

        resolve(status)
      })
    })
  }

  const saveProduct = (payload) => {
    return new Promise((resolve, reject) => {
      productCollection.insertOne(payload, (err,product) => {
        if (err) reject(new Error('an error occured registring a save product, err:', err))

        resolve(product)
      })
    })
  }

  const getProductById = (product) => {
    return new Promise((resolve, reject) => {
      const query = { productId: product.productId };
      const response = (err,order) => {
        if(err) reject(new Error('an error occured registring a product, err:', err))

          resolve(analyzerForked.send({msg:'analize', product}));
      }

      productCollection.findOne(query, {}, response)
    })
  }


  // analyze()
  const disconnect = () => {
    db.close()
  }

  return Object.create({
    disconnect,
    analyze
  })
}

const connect = (container) => {
  return new Promise((resolve, reject) => {
    if (!container.resolve('database')) {
      reject(new Error('connection db not supplied!'))
    }
    resolve(repository(container))
  })
}

module.exports = Object.assign({}, {connect})
