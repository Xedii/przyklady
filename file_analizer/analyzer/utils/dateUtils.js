const moment = require('moment');

module.exports.enumerateDaysBetweenDates = (startDate, endDate) => {
  let now = startDate.clone(), dates = [];

  while (now.isBefore(endDate) || now.isSame(endDate)) {
    dates.push(now.format('M-DD-YYYY'));
    now.add(1, 'days');
  }
  return dates;
}
