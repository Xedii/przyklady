const utils = require('./Maxim')
const should = require('should')
const assert = require('assert');
const product = require('../mock/product.json')
const sortedDate = require('../mock/date.json')
const sortDate = require('../mock/sortDate.json')
const countValue = require('../mock/countValue.json')
const outLier = require('../mock/outLier.json')


describe("Check utils results", () => {

    it("check max value", () => {
        const maxValue = 10.99;
        utils.getMax(product.priceTracking).should.equal(maxValue)
    });

    it("check min value", () => {
        const minValue = 1.51;
        utils.getMin(product.priceTracking).should.equal(minValue)
    });

    it("check sorted data", () => {
        JSON.stringify(utils.sortedData(product.priceTracking)).should.be.equal(JSON.stringify(sortedDate))
    });

    it("check avg value", () => {
        const avgValue = 1.84;
        const sortData = utils.sortedData(product.priceTracking);
        utils.getAvg(sortData).should.equal(avgValue)
    });

    it("check getStDev value", () => {
        const stDevValue = 1.69;
        const sortData = utils.sortedData(product.priceTracking);
        utils.getStDev(sortData).should.containEql(stDevValue)
    });

    it("check sort date", () => {
      assert.deepEqual(utils.sortDate(product.priceTracking), sortDate)
    });

    it("check count value", () => {
        const sortData = utils.sortedData(product.priceTracking);
        assert.deepEqual(utils.count(sortData), countValue)
    });

    it("check that filter out liers works fine", () => {
        const sortData = utils.sortedData(product.priceTracking);
        assert.deepEqual(utils.filterOutliers(sortData), outLier)
    });
});
