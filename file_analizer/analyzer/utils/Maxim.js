const { enumerateDaysBetweenDates } = require('./dateUtils')
const moment = require('moment');
const math = require('mathjs');

const getMax = (priceHistory) =>
  priceHistory.reduce((min, p) => p.price > min ? p.price : min, priceHistory[0].price)

const getMin = (priceHistory) =>
  priceHistory.reduce((min, p) => p.price < min ? p.price : min, priceHistory[0].price)

const sortedData = (priceHistory) => {
  const startDate = date => moment(date.startDate, "M-DD-YYYY");
  const endDate = date =>  moment(date.endDate, "M-DD-YYYY");
  const price = date =>  date.price;

  const daysBeetweenDate = (date) => ({dates: enumerateDaysBetweenDates(startDate(date), endDate(date)), price: price(date)})
  const dataHistory = priceHistory.map(daysBeetweenDate)

  return dataHistory.map(data => data.dates.map(obj => ({date:obj, price: data.price})))
  .reduce((a,b) => [...a,...b])
  .sort((a,b) => new Date(a.date) - new Date(b.date))
}

const getAvg = (sortedData) => {
  const average = arr => arr.map(obj => obj.price)
  	.reduce((p, c) => p + c, 0) / arr.length;
  return Math.round(average(sortedData) * 100)/100
}

const getStDev = (sortedData) => {
  const getPrice = data => data.price
  return math.std(sortedData.map(getPrice)).toFixed(2)
}

const sortDate = data => data.sort((a,b) => new Date(a.date) - new Date(b.date))

const count = names =>
  names.reduce((a, b) =>
    Object.assign(a, {[b.price]: (a[b.price] || 0) + 1}), {})

const filterOutliers = (someArray) => {
  if(someArray.length < 4) return someArray;

  let q1, q3;
	const getPrice = data => data.price
  const values = someArray.slice().map(getPrice)
      .sort((a,b) => a - b)

  if((values.length / 4) % 1 === 0){
    q1 = 1/2 * (values[(values.length / 4)] + values[(values.length / 4) + 1]);
    q3 = 1/2 * (values[(values.length * (3 / 4))] + values[(values.length * (3 / 4)) + 1]);
  } else {
    q1 = values[Math.floor(values.length / 4 + 1)];
    q3 = values[Math.ceil(values.length * (3 / 4) + 1)];
  }

  const iqr = q3 - q1;
  const maxValue = q3 + iqr * 1.5;
  const minValue = q1 - iqr * 1.5;

	return someArray.filter((x) => (x.price <= maxValue) && (x.price >= minValue));
}

module.exports = Object.assign({}, {
  getMax,
  getMin,
  sortedData,
  getAvg,
  getStDev,
  count,
  sortDate,
  filterOutliers
})
