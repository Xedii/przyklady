const STATUS = {
    NOT_PROMOTION : {status: 'NOT_PROMOTION', category : 0},
    BEST_PRICE : {status: 'BEST_PRICE', category : 1},
    BETTER_CONST_PRICE : {status: 'BETTER_CONST_PRICE', category : 2},
    NOW_LOW_CONST_PRICE : {status: 'NOW_LOW_CONST_PRICE', category : 3},
    AVG_BETTER_PRICE : {status: 'AVG_BETTER_PRICE', category : 4},
    EXPENSIVE_CHEAP : {status: 'EXPENSIVE_CHEAP', category : 5},
};

module.exports = Object.assign({}, {STATUS})
