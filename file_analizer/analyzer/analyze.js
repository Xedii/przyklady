'use strict'
const { status } = require('./model')
const { dataAnalyze } = require('./utils')

const checkPromotion = (product, detailProduct) => {
  const dayStartAggregate = 10;
  const aggregateDateCheck = 4;
  const divideHalf = 2;
  const startZero = 0;
  const minusOne = 1;
  //sortuje by wziąść z tego ostatnią datę z ceną
  product.priceTracking = dataAnalyze.sortDate(product.priceTracking)
  //ostatnia data
  const priceTrackingDate = product.priceTracking[product.priceTracking.length - minusOne];
  //sprawdza cenę ostatniej daty ieśli jest inna to dodaje nowy obiekt
  if(priceTrackingDate.price != detailProduct.price) {
    const priceTracking = {
      startDate: moment().format('M-DD-YYYY'),
      endDate: moment().format('M-DD-YYYY'),
      price: detailProduct.price
    }
    product.priceTracking.push(priceTracking)
  } else {
    product.priceTracking[product.priceTracking.length - minusOne].endDate = moment().format('M-DD-YYYY');
  }
  //sprawdza ostatnie 10 dni czy cena się rózni
  const priceIsDiff = data => data.price === detailProduct.price
  const isTrue = element => element === true
  const lastTenDayDiff = historyByDay.slice(Math.max(historyByDay.length - dayStartAggregate, startZero))
    .map(priceIsDiff).every(isTrue)

  if(detailProduct.price <= product.constPrice || product.priceTracking.length > aggregateDateCheck || lastTenDayDiff) {
    product.analisePrice = status.NOT_PROMOTION
    return product
  }

  //tworzy obiekt z datami i cenami pojedynczymi
  const priceTracking = dataAnalyze.sortedData(product.priceTracking)
  product.constPrice = dataAnalyze.getStDev(priceTracking);

  const productMaxPrice = dataAnalyze.getMax(product.priceTracking)
  const productMinPrice = dataAnalyze.getMin(product.priceTracking)

  const isBestPrice = detailProduct.price < productMinPrice
  const isBetterConst = detailProduct.price < product.constPrice

  //sprawdza połowe historii cen i wyciąga z tego najczęściej powtarzającą się wartość
  const half_arr = Math.max(historyByDay.length/divideHalf, startZero)
  const RepeatPrices = dataAnalyze.count(historyByDay.slice(half_arr))
  const mostRepeatPrice = Object.keys(RepeatPrices)
  .reduce((a, b) => repeat[a] > repeat[b] ? a : b )

  const isLessThenRepeat = detailProduct.price <= parseFloat(mostRepeatPrice)

  if (isBestPrice) product.analizaPrice = status.BEST_PRICE
  else if (isLessThenRepeat) product.analizaPrice = status.BETTER_CONST_PRICE
  else if (isBetterConst) product.analizaPrice = status.NOW_LOW_CONST_PRICE
  else product.analizaPrice = status.NOT_PROMOTION

  return product
}

module.exports = Object.assign({}, {checkPromotion})
