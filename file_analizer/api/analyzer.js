'use strict'
const status = require('http-status')

module.exports = ({repo}, app) => {
  app.get('/analyze/:id/type/:type', (req, res, next) => {
    repo.analyze(req.params.id, req.params.type)
      .then(progress => res.status(status.OK).json(progress))
      .catch(order => res.status(status.NOT_FOUND).json(order))
  })
}
